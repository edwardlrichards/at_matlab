function envFilePath = write_eENVBell(c, z, varargin)

%{
Writes the variables in this file to a bellhop enviornment. The .env file
will be saved to a director in the same folder as this file. The directory
name is determined by run type (A,E, etc.).
%}

%% Default values
%Name is the title of the run
name='test';
% Location where the save directory is located
saveDir = '.';
% Source Depth
sourceD=93.5;
% Recieve depth
receiveD=95;
% Recive range
range=3;
znssp = 100;

% User specified parameters
k = 1;
nVarargs = numel(varargin);

while k <= nVarargs
    switch varargin{k}
        case 'title'
            name = varargin{k + 1};
            k = k + 2;
        case 'saveDir'
            saveDir = varargin{k + 1};
            k = k + 2;
        case 'sourceD'
            sourceD = varargin{k + 1};
            k = k + 2;
        case 'receiveD'
            receiveD = varargin{k + 1};
            k = k + 2;
        case 'range'
            range = varargin{k + 1};
            k = k + 2;
        case 'bottomDepth'
            znssp = varargin{k + 1};
            k = k + 2;
        otherwise
            error('option name not recognized')
    end
end

%o3 is the model run type, E is for eigen ray
o3='E';

%Open the target file
saveName=[name,'_',o3];
title=['''',name,' ',o3,''''];

%Dummy variable in Bellhop
%Frequency in Hz
frequency=10000;
nmedia=1;
nmesh=51;
sigmas=0;

%Options 1
o1='CVF';

%Bottom Block
o2='A';
sigmab=0;
bottom=[znssp 1600 0 1.8 0.8];

% numbers of things
nSources=numel(sourceD);
gridH=numel(range);
gridV=numel(receiveD);

% Test eigen rays, spend as much as you can
numAngles=5e4;
theta=[-14 14];

envDir=[saveDir, 'envE/'];

% Bound the problem
rayStep=0;
zmax=znssp*1.1;
rmax=range(end)*1.1;

%Make sure the the deepest sound speed is above the bottom
index=find(z<=znssp);
profile=[z(index);c(index)];

%Select the CTD file
%Create the save directory if it doesn't exist

if ~exist(envDir,'dir')
    disp(['Creating save directory : ',envDir]);
    mkdir(envDir);
end

envPath = [envDir, saveName];
fid=fopen([envPath,'.env'],'w+');

%% Parse out the file
fprintf(fid,'%s 		\t! TITLE\n',title);
fprintf(fid,'%.1f    \t! FREQ (HZ)\n',frequency);
fprintf(fid,'%u     \t! NMEDIA\n',nmedia);
fprintf(fid,'''%s''  	\t! SSPOPT (Analytic or C-linear interpolation)\n',o1);
fprintf(fid,'%u %.1f %.2f 		\t! Depth of bottom (m)\n',nmesh,sigmas,znssp);
fprintf(fid,'  %-.3f %-.3f / \n',profile);
fprintf(fid,'''%s'' %.2f \n',o2,sigmab);
fprintf(fid,' %.2f %.1f %.1f %.1f %.1f/\n',bottom);

%Cases below allow for 1 or more sources and recievers
if nSources==1
    fprintf(fid,'%u \t! NSD\n%.2f/		\t! SD(1:NRD) (m)\n',nSources,sourceD);
else
    fprintf(fid,'%u \t! NSD\n%.2f %.2f/		\t! SD(1:NRD) (m)\n',nSources,sourceD);
end

if gridV==1
    fprintf(fid,'%u \t! NRD\n%.2f/   \t! RD(1:NRD)\n',gridV,receiveD);
else
    fprintf(fid,'%u \t! NRD\n%.2f %.2f/   \t! RD(1:NRD)\n',gridV,receiveD);
end

if gridH==1
    fprintf(fid,'%u\t ! NR\n%.2f/ \t! R(1:NR) (km) \n',gridH,range);
else
    fprintf(fid,'%u \t! NR\n%.2f %.2f/ \t! R(1:NR) (km) \n',gridH,range);
end

fprintf(fid,'''%s'' \t! R/C/I/S/E/A \n',o3);
fprintf(fid,'%u 	\t! NBeams\n%.2f %.2f / ! ALPHA1,2 (degrees)\n',numAngles,theta);
fprintf(fid,'%.2f %.2f %.2f 	\t	! STEP (m), ZBOX (m), RBOX (km)\n', rayStep,zmax,rmax);
fclose(fid);

%% Return value
envFilePath = envPath;