function plotArrivalVDepth(ARRFIL, bellhopParam)
% plot the arrivals calculated by BELLHOP
% irr = index of receiver range
% ird = index of receiver depth
% isd = index of source   depth
%Modified from acoustic toolbox code by Edward Richards, 2011

recieveRangeI = 1;
sourceDepthI = 1;

minSS = 1526;
maxSS = 1538;

% limit the number of surface bounces plotted
maxNumTopBounce = 2;

% Read data from arrival file
Narrmx = 1e3;
[Arrivals, Positions] = read_arrivals_asc(ARRFIL, Narrmx);

numArrivals = Arrivals.Narr;
arrivalI = 1 : numArrivals;

plotAmplitude = squeeze(abs(Arrivals.A(recieveRangeI, arrivalI, :)));
plotTime = squeeze(Arrivals.delay(recieveRangeI, arrivalI, :));
plotAngle = squeeze(Arrivals.RcvrAngle(recieveRangeI, arrivalI, :));
numTopBounce = squeeze(Arrivals.NumTopBnc(recieveRangeI, arrivalI, :));
numBotBounce = squeeze(Arrivals.NumBotBnc(recieveRangeI, arrivalI, :));

[maxAmp, maxAmpI] = max(plotAmplitude);
[maxDepth, maxDepth] = max(max(plotAmplitude));
timeMaxes = plotTime(maxAmpI);
maxOfAll = max(maxAmp);
plotTime = (plotTime - timeMaxes(1)) * 1000;

depths = Positions.r.depth;

%This is the all important part of the code where acuall plotting happens
figure;
h=subplot(1,2,1);

z=bellhopParam.z;
cp=bellhopParam.c;
plot(cp,z);
set(gca,'YDir','reverse');
ylabel('depth, m'); xlabel('sound speed, m/s');

%Make the x axis consistant for simple comparison
axis([minSS maxSS 0 100]);
xT = minSS : 4 : maxSS;
set(h,'xTick',xT);

xTL = cellstr(num2str(xT.'));
for i = 1 : numel(xTL)
    if mod(i, 2)
        xTL{i} = ' ';
    end
end
set(h,'xTickLabel',xTL);

n=subplot(1,2,2);
set(n, 'YDir', 'Reverse' )   % plot with depth-axis positive down
hold on

for i = 1 : numel(depths)
    offsetCIR = depths(i) - (plotAmplitude(:, i) / maxOfAll) * 6;
    %     Add a minute zero on both left and right of all arrivals. Totally
    %     hack way to simulate a stem plot
    tempTime = [plotTime(:, i) - eps, plotTime(:, i), plotTime(:, i) + eps];
    tempTime = reshape(tempTime.', numel(tempTime), 1);
    tempDepth = depths(i) * ones(size(plotAmplitude(:, i)));
    offsetCIR = [tempDepth, offsetCIR, tempDepth];
    offsetCIR = reshape(offsetCIR.', numel(offsetCIR), 1);
    plot(tempTime, offsetCIR);
end

xlabel( 'Time (ms)' )
% ylabel( 'Depth, m' )
title( [ 'Sd = ', num2str( Positions.s.depth( sourceDepthI ) ), ...
    ' m    Rr = ', num2str( Positions.r.range( recieveRangeI ) ), ' m' ] )
xlim([bellhopParam.tFastRange(1), bellhopParam.tFastRange(2)]);

hold off
zoom on

set(h,'Position',[.1 .15 .15 .75]);
set(n,'Position',[.38,.15,.58,.75]);
% 
% % size the plot
% set( gcf, 'Units', 'centimeters' )
% set( gcf, 'ActivePositionProperty', 'Position', 'Units', 'centimeters' )
% set( gcf, 'Position', [ 3 15 19.0 10.0 ] )

