function c_Bellhop(bellhopParam)
%% Create an enviormental file
% There are a few parameters of great importance in the writting of the
% enviormental file that are not set in this main script. The type of the
% bellhop run, bottom parameters, etc. are all set inside writeENVBell.m

envFilePathC = write_ENVBell(bellhopParam.c, bellhopParam.z, 'C',...
    'saveDir', bellhopParam.saveDir, 'sourceD', bellhopParam.sourceD,...
    'title', bellhopParam.name, 'receiveD', bellhopParam.receiveD,...
    'range', bellhopParam.range, 'bottomDepth', bellhopParam.bottomDepth);

%% run Bellhop
% writeENVBell has created an .env file at envPath. Run Bellhop on this
% file
% Macs may need to include the incatation
% setenv('DYLD_LIBRARY_PATH', '/usr/local/bin/');
bellhop(envFilePathC);

%% Plot Bellhop output
plotssp(envFilePathC);

if numel(bellhopParam.receiveD) == 1
    plotSpark_singleChannel(envFilePathC, bellhopParam);
else
    plotSpark_withSSP(envFilePathC, bellhopParam);
end

%% Print output
% Modify name and location to taste

% print('-dpng', [bellhopParam.saveDir,'figs/spark',...
%     bellhopParam.name]);

