function KAM11_cirBellhop(bellhopParam)
%% Create an enviormental file
% There are a few parameters of great importance in the writting of the
% enviormental file that are not set in this main script. The type of the
% bellhop run, bottom parameters, etc. are all set inside writeENVBell.m
plotType = 'spark/';

envFilePathA = write_ENVBell(bellhopParam.c, bellhopParam.z, 'A',...
    'saveDir', bellhopParam.saveDir, 'sourceD', bellhopParam.sourceD,...
    'title', bellhopParam.name, 'receiveD', bellhopParam.receiveD,...
    'range', bellhopParam.range, 'bottomDepth', bellhopParam.bottomDepth);

%% run Bellhop
% writeENVBell has created an .env file at envPath. Run Bellhop on this
% file
% Macs may need to include the incatation
% setenv('DYLD_LIBRARY_PATH', '/usr/local/bin/');
bellhop(envFilePathA);

%% Plot Bellhop output
% ray trace is not be appropriate for all types of Bellhop runs
plotssp(envFilePathA);

if numel(bellhopParam.receiveD) == 1
    %     plotBellCIR(envFilePathA, bellhopParam);
    plotSpark_singleChannel(envFilePathA, bellhopParam);
else
    %     plotArrivalVDepth(envFilePathA, bellhopParam);
    plotSpark_withSSP(envFilePathA, bellhopParam);
end

%% Save the output
if ~exist([bellhopParam.saveDir, plotType],'dir')
    disp(['Creating save directory : ',bellhopParam.saveDir, plotType]);
    mkdir([bellhopParam.saveDir, plotType]);
end

print('-dpng', [bellhopParam.saveDir, plotType, bellhopParam.name]);
