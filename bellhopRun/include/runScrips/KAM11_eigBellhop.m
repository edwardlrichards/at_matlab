function KAM11_eigBellhop(bellhopParam)
%% Create an enviormental file
% There are a few parameters of great importance in the writting of the
% enviormental file that are not set in this main script. The type of the
% bellhop run, bottom parameters, etc. are all set inside writeENVBell.m
plotType = 'eig/';

envFilePathE = write_ENVBell(bellhopParam.c, bellhopParam.z, 'E',...
    'saveDir', bellhopParam.saveDir, 'sourceD', bellhopParam.sourceD,...
    'title', bellhopParam.name, 'receiveD', bellhopParam.receiveD,...
    'range', bellhopParam.range, 'bottomDepth', bellhopParam.bottomDepth);

%% run Bellhop
% writeENVBell has created an .env file at envPath. Run Bellhop on this
% file
% Macs may need to include the incantation
% setenv('DYLD_LIBRARY_PATH', '/usr/local/bin/');
bellhop(envFilePathE);

%% Plot Bellhop output
% ray trace is not be appropriate for all types of Bellhop runs
% plotssp(envFilePathE);
plotEig_withSSP(envFilePathE,...
    bellhopParam.receiveD(bellhopParam.eigChannel), 3.3);

%% Save the output

if ~exist([bellhopParam.saveDir, plotType], 'dir')
    disp(['Creating save directory : ',bellhopParam.saveDir, plotType]);
    mkdir([bellhopParam.saveDir, plotType]);
end

print('-dpng', [bellhopParam.saveDir, plotType, bellhopParam.name]);