function [z, c] = getSS_WHOI_TS(measurementDateNumber, bottomDepth)
% compute and return the sound speed profile from the tempurature data
% recorded on WHOI TS, KAM11.

tempProfile = getTempuratureProfile(measurementDateNumber);
salinityProfile = tempToSalinity(tempProfile);
depthProfile = getTS_Depth;

%% Create depth and sound speed array
%Define the sound speed profile. It is important that both these profiles
%are horizontal vectors. (1 x n). It is also important that the ssp be
%defined at z=0 and z=bottom depth
% Assume constant temp and salinity at top
tempProfile = [tempProfile(1), tempProfile];
salinityProfile = [salinityProfile(1), salinityProfile];
depthProfile = [0, depthProfile];

c = snd_spd(depthProfile, tempProfile, salinityProfile);

% Assume constant sound speed to bottom
z = [depthProfile, bottomDepth];
c = [c, c(end)];
% cp = interp1(depthProfile, c, z,'linear', 'extrap');
