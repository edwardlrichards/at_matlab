function createStarmonMAT

dataDir = '/Users/edwardlrichards/Documents/data/KAM11/enviornment/WHOI_Temp_String/TemperatureString/';

TlogList = { '1T3870.DAT', '1T3871.DAT', '1T3922.DAT', '1T3866.DAT',...
    '1T3865.DAT', '1T3917.DAT', '1T3920.DAT', '1T3924.DAT', '1T3914.DAT',...
    '1T3918.DAT', '1T3916.DAT', '1T3867.DAT', '2T3915.DAT', '1T3869.DAT'};

starmonDepths = [20 25 30 35 40 45 50 55 60 65 70 80 85 90 ];

% 734695.8750000 734695.8750579  5 sec diff
%    seabird loggers record 15 sec interval
% interpolate data to 6/24 06:00:00  -> 7/11 19:45:00 with 15 sec interval
dtimeI = datenum(2011,6,24,06,00,00):15/86400:datenum(2011,7,11,19,45,00);

rem = dtimeI - floor(dtimeI);
hours = floor(24 * rem);
rem = 24 * rem - hours;
minutes = floor( 60 * rem );
rem = 60 * rem - minutes;
seconds = 60 * rem;

[dates, ia, ic] = unique(floor(dtimeI));
jDay = datevec2doy(datevec(dtimeI(ia)));

starmonTemp = zeros(numel(dtimeI), 14);
starmonTime = dtimeI;
starmonDOY = zeros(numel(dtimeI), 4);

starmonDOY(:, 1) = jDay(ic);
starmonDOY(:, 2) = hours;
starmonDOY(:, 3) = minutes;
starmonDOY(:, 4) = seconds;

for k = 1 : length(TlogList),
    fname = char(TlogList(k));
    fdir = fname(2:6);
    fid = fopen([dataDir, fdir, '/', char(TlogList(k))] );
    
    % loop until data starts
    j = fgetl(fid);
    while ( isempty( strfind(j,'OTCR:') ) )
        j = fgetl(fid);
    end
    
    %   now read in data
    %  count   date   time     temp      21,804
    d = fscanf(fid,'%d %d.%d.%d %2d:%2d:%2d  %d,%d');
    d = reshape(d,9,length(d)/9).';
    
    dtime = datenum(2000+d(:,4),d(:,3),d(:,2),d(:,5),d(:,6),d(:,7));

    dtemp = d(:,8)+d(:,9)/1000; %  convert decimal temp
    
    if ( k == 1 ) % clean up glitches in T3870 data
        indx = find(dtemp > 26.5 );
        dtemp(indx) = [];
        dtime(indx) = [];
    end
    
    % interp data so all logger have same number of samples
    dtempI = interp1(dtime, dtemp, dtimeI);
    
    starmonTemp(:,k) = dtempI;
end
save('starmonTemps', 'starmonDepths', 'starmonTemp', 'starmonTime',...
    'starmonDOY');