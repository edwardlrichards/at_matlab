function [tempProfile, measureDateNum] =...
    getTempuratureProfile(datenumOfInterest, starmonTemp, starmonTime)
% Check whether there is a nice mat file sitting around waiting to be
% loaded. If not, create it

if exist('starmonTemps.mat', 'file')
    load starmonTemps.mat
else
    createStarmonMAT
    load starmonTemps.mat
end

indexOfExecdence = starmonTime >= datenumOfInterest;
tempData = starmonTemp(indexOfExecdence, :);
tempProfile = tempData(1, :);
tempTime = starmonTime(indexOfExecdence);
measureDateNum = tempTime(1);