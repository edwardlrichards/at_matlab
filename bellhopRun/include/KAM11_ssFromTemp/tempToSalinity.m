function salinity = tempToSalinity(tempurature)
% Lookup salinity value from tempurature. This lookup table is caluclated
% from KAM 11 CTD data

load('CvTCurve');
salinity = zeros(size(tempurature));
salinity(tempurature < CvT(1, 1)) = CvT(1, 2);
salinity(tempurature > CvT(end, 1)) = CvT(end, 2);
otherwiseIndex = tempurature >= CvT(1, 1) & tempurature <= CvT(end, 1);
salinity(otherwiseIndex) =...
    interp1(CvT(:,1), CvT(:,2), tempurature(otherwiseIndex));