function plotEig_withSSP(rayFile, depth, tolerance)
% Plot all rays that are within tolerance of depth.

%Added a ssp profile on the left side of plot, ELR
% Plot the RAYfil produced by Bellhop
% usage: plotray( rayfil )
% where rayfil is the ray file (extension is optional)
% e.g. plotray( 'foofoo' )
%

% MBP July 1999

units ='m';
jkpsflag = 1;

minSS = 1526;
maxSS = 1538;

% saveName=rayFile;

[ ~, ~, SSP, ~, fid] = read_env( rayFile, 'KRAKEN' ); % read in the environmental file

if ( strcmp( rayFile, 'RAYFIL' ) == 0 && isempty( findstr( rayFile, '.ray' ) ) )
    rayFile = [ rayFile '.ray' ]; % append extension
end

% plots a BELLHOP ray file

fid = fopen( rayFile, 'r' );   % open the file
if ( fid == -1 )
    disp( rayFile );
    errordlg( 'No ray file exists; you must run BELLHOP first (with ray ouput selected)', 'Error' );
end

% read header stuff

TITLE  = fgetl(  fid );
FREQ   = fscanf( fid, '%f', 1 );
NBEAMS = fscanf( fid, '%i', 1 );
DEPTHT = fscanf( fid, '%f', 1 );
DEPTHB = fscanf( fid, '%f', 1 );

% Extract letters between the quotes
nchars = strfind( TITLE, '''' );   % find quotes
TITLE  = [ TITLE( nchars( 1 ) + 1 : nchars( 2 ) - 1 ) blanks( 7 - ( nchars( 2 ) - nchars( 1 ) ) ) ];
TITLE  = deblank( TITLE );  % remove whitespace

% read rays
beams = cell(NBEAMS, 1);

for i = 1 : NBEAMS
    alpha0    = fscanf( fid, '%f', 1 );
    nsteps    = fscanf( fid, '%i', 1 );
    beams{i}.NumTopBnc = fscanf( fid, '%i', 1 );
    beams{i}.NumBotBnc = fscanf( fid, '%i', 1 );
    if isempty(nsteps); break; end
    ray = fscanf( fid, '%f', [2 nsteps] );
    beams{i}.r = ray(1, : );
    beams{i}.z = ray(2, : );
end

beams = beams(cellfun(@(beam) isfield(beam, 'r'), beams));
tempBeam = {};
for i = 1 : numel(depth)
    tempBeam = [tempBeam; beams(cellfun(@(beam)...
        abs(beam.z(end) - depth(i)) < tolerance, beams))];
end
beams = tempBeam;

%This is the all important part of the code where actual plotting happens
figure;
h=subplot(1,2,1);

z=SSP.z;
cp=SSP.c;
plot(cp,z);
set(gca,'YDir','reverse');
grid on;
ylabel('depth, m','FontSize',16); xlabel('sound speed, m/s','FontSize',16);

%Make the x axis consistant for simple comparison
axis([minSS maxSS 0 100]);
xT = minSS : 4 : maxSS;
set(h,'xTick',xT);

xTL = cellstr(num2str(xT.'));
for i = 1 : numel(xTL)
    if mod(i, 2)
        xTL{i} = ' ';
    end
end
set(h,'xTickLabel',xTL);
set(h,'Position',[.1 .15 .15 .8]);

%% Beam plot
n=subplot(1,2,2);
set(n, 'YDir', 'Reverse' )   % plot with depth-axis positive down
hold on

% axis limits
rmin = +1e9;
rmax = -1e9;
zmin = +1e9;
zmax = -1e9;

for i = 1 : numel(beams)
    if ( strcmp( units, 'km' ) )
        beams{i}.r = beams{i}.r / 1000;   % convert to km
    end

    if beams{i}.NumTopBnc >= 1
        plot(beams{i}.r, beams{i}.z, 'k','LineWidth',1  )	% hits surface only
    elseif beams{i}.NumBotBnc >= 1
        plot(beams{i}.r, beams{i}.z, 'b','LineWidth',1  )	% hits bottom only
    elseif beams{i}.NumTopBnc <=3
        plot(beams{i}.r, beams{i}.z, 'r','LineWidth',1 )
    end
    
    % update axis limits
    rmin = min( [beams{i}.r rmin ] );
    rmax = max( [beams{i}.r rmax ] );
    zmin = min( [beams{i}.z zmin ] );
    zmax = max( [beams{i}.z zmax ] );
    if ( zmin == zmax ) % horizontal ray causes axis scaling problem
        zmax = zmin + 1;
    end
    axis( [ rmin, rmax, zmin, zmax ] )
    
    if rem( i, fix( NBEAMS / 10 ) ) == 0,    % flush graphics buffer every 10th ray
        drawnow
    end;
end


xlabel( 'Range (m)','FontSize',16 )
if ( strcmp( units, 'km' ) )
    xlabel( 'Range (km)','FontSize',16 )
end

fclose( fid );

hold off
zoom on

set(n,'Position',[.31,.15,.65,.8]);

if ( nargout == 1 )
    varargout{ 1 } = findobj( 'Type', 'Line' );   % return a handle to the lines in the figure
end

% set up axis lengths for publication
if ( jkpsflag )
    set( gcf, 'Units', 'centimeters' )
    set( gcf, 'ActivePositionProperty', 'Position', 'Units', 'centimeters' )
    
    set( gcf, 'Position', [ 2 2 14.0  7.0 ] )
    %set( gcf, 'PaperPosition', [ 3 3 19.0 10.0 ] )
end

% fixed size for publications
if ( jkpsflag )
    set( gcf, 'Units', 'centimeters' )
    set( gcf, 'Position', [ 2 2 14.0  7.0 ] )
    set(gcf, 'PaperPositionMode', 'auto');
    
    %set( gcf, 'Units', 'centimeters' )
    %set( gcf, 'PaperPosition', [ 3 3 19.0 10.0 ] )
    set( gcf, 'Units', 'centimeters' )
    set( gcf, 'Position', [ 3 15 19.0 10.0 ] )
end

% print(gcf,['~/Documents/internship11/MATLAB/atGraphs/Bellhop/envE/',saveName],'-depsc2');