function drawSparkLine_cell(axisHandle, time, amplitude, yCenter, yScale, color)

for i = 1 : numel(time)
    length = amplitude(i) * yScale;
    h = line([time(i), time(i)],...
        [yCenter - (length / 2), yCenter + (length / 2)]);
    set(h, 'Parent', axisHandle);
    set(h, 'Color', color{i});
    set(h, 'LineWidth', 1);
end