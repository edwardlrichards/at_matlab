%% Bellhop run example
% Compare Bellhop results from full CTD casts to sound speed profiles
% infered from temperature string data.
% Data taken during the KAM11 experiment, this code example
% was written by Edward Richards
close all; clear; clc;

ctdCastNum = 6;
ctdPath = '../include/sspData/KAM11_totalSSP';
% save directory
bellhopParam.saveDir = '../output/figs/';

load(ctdPath);
addpath(genpath('../include'));

dataChannel = 1 : 16;
bellhopParam.eigChannel = 12;

bellhopParam.measurementDateNumber = datenum(...
    sspProfiles{ctdCastNum}.time);

% Make the title of the run file
jDay = datevec2doy(datevec(bellhopParam.measurementDateNumber));
dayTime = datestr(bellhopParam.measurementDateNumber, 'HH_MM');
if numel(dataChannel) > 1
    channelName = [num2str(min(dataChannel)), '_',...
        num2str(max(dataChannel))];
else
    channelName = num2str(dataChannel);
end

bellhopParam.name = ['KAM11_', num2str(jDay), '_', dayTime];

bellhopParam.bottomDepth = 106;

bellhopParam.range = 3;
bellhopParam.tFastRange = [-7.5, 3.5];

%% Reciver and source arrays
numSource = 8;
firstSource = 8.5;
sourceSpace = 7.5;
bellhopParam.sourceD = bellhopParam.bottomDepth -...
    ((0 : (numSource - 1)) * sourceSpace + firstSource);

% Only run one source depth at a time
bellhopParam.sourceD = bellhopParam.sourceD(1);

numPhones = 16;
firstPhone = 8.6;
phoneSpace = 3.75;
bellhopParam.receiveD = bellhopParam.bottomDepth -...
    ((0 : (numPhones - 1)) * phoneSpace + firstPhone);
bellhopParam.receiveD = bellhopParam.receiveD(dataChannel);

%% Sound speed profile
% Load WHOI tempurature data and calculate SSP from there
bellhopParam.c = sspProfiles{ctdCastNum}.data.soundSpeed.';
bellhopParam.z = sspProfiles{ctdCastNum}.data.depth.';

% ensure ssp extends to the bottom
bellhopParam.z = [bellhopParam.z, bellhopParam.bottomDepth];
bellhopParam.c = [bellhopParam.c, bellhopParam.c(end)];
%% Run simulation
% we have 2 titles
tempName = bellhopParam.name;

bellhopParam.name = [tempName, '_chan', channelName, '_CTD'];
KAM11_cirBellhop(bellhopParam);

bellhopParam.name = [tempName, '_chan',...
    num2str(bellhopParam.eigChannel), '_CTD'];
KAM11_eigBellhop(bellhopParam);

%% Load WHOI tempurature data and calculate SSP from there
[bellhopParam.z, bellhopParam.c] = getSS_WHOI_TS(...
    bellhopParam.measurementDateNumber, bellhopParam.bottomDepth);

%% Run simulation
% we have 2 titles
bellhopParam.name = [tempName, '_chan', channelName,'_TS'];
KAM11_cirBellhop(bellhopParam);

bellhopParam.name = [tempName, '_chan',...
    num2str(bellhopParam.eigChannel), '_TS'];
KAM11_eigBellhop(bellhopParam);