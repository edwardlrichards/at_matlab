function rayData = parse_rayBell(filePath)
% Parse a ray file.
% filePath works both with and without an extension

if ( strcmp( filePath, 'RAYFIL' ) == 0 && isempty( findstr( filePath, '.ray' ) ) )
    filePath = [ filePath '.ray' ]; % append extension
end

fid = fopen( filePath, 'r' );   % open the file
if ( fid == -1 )
    disp( filePath );
    errordlg( 'No ray file exists; you must run BELLHOP first (with ray ouput selected)', 'Error' );
end

% read header stuff

TITLE  = fgetl(  fid );
rayData.FREQ   = fscanf( fid, '%f', 1 );
NBEAMS = fscanf( fid, '%i', 1 );
rayData.DEPTHT = fscanf( fid, '%f', 1 );
rayData.DEPTHB = fscanf( fid, '%f', 1 );

% Extract letters between the quotes
nchars = strfind( TITLE, '''' );   % find quotes
TITLE  = [ TITLE( nchars( 1 ) + 1 : nchars( 2 ) - 1 ) blanks( 7 - ( nchars( 2 ) - nchars( 1 ) ) ) ];
rayData.TITLE  = deblank( TITLE );  % remove whitespace

% read rays
beams = cell(NBEAMS, 1);

for i = 1 : NBEAMS
    alpha0    = fscanf( fid, '%f', 1 );
    nsteps    = fscanf( fid, '%i', 1 );
    beams{i}.NumTopBnc = fscanf( fid, '%i', 1 );
    beams{i}.NumBotBnc = fscanf( fid, '%i', 1 );
    if isempty(nsteps); break; end
    ray = fscanf( fid, '%f', [2 nsteps] );
    beams{i}.r = ray(1, : );
    beams{i}.z = ray(2, : );
end

rayData.beams = beams;



% 
% beams = beams(cellfun(@(beam) isfield(beam, 'r'), beams));
% tempBeam = {};
% for i = 1 : numel(depth)
%     tempBeam = [tempBeam; beams(cellfun(@(beam)...
%         abs(beam.z(end) - depth(i)) < tolerance, beams))];
% end
% beams = tempBeam;
