function plotBellCIR(ARRFIL, bellhopParam)
% Assume there is only 1 channel that we care about

measurementDateNumber = bellhopParam.measurementDateNumber;
% limit the number of surface bounces plotted
maxNumTopBounce = 2;
minPlotDB = -40;

Narrmx = 1e3;
[Arrivals, Positions] = read_arrivals_asc(ARRFIL, Narrmx);

numArrivals = Arrivals.Narr;
arrivalI = 1 : numArrivals;

plotAmplitude = abs(Arrivals.A(arrivalI));
plotTime = Arrivals.delay(arrivalI);
plotAngle = Arrivals.RcvrAngle(arrivalI);
numTopBounce = Arrivals.NumTopBnc(arrivalI);
numBotBounce = Arrivals.NumBotBnc(arrivalI);

% Index out the arrivals based on the number of bounces
noBounceI = (numTopBounce == 0 & numBotBounce == 0);
noTopI = (numTopBounce == 0 & numBotBounce > 0);
topI = (numTopBounce ~= 0 & numTopBounce <= maxNumTopBounce);

[maxAmp, maxAmpI] = max(plotAmplitude);
plotTime = (plotTime - plotTime(maxAmpI)) * 1000;

% dB scale
plotAmplitudeDB = 20 * log10(abs(plotAmplitude));
plotAmplitudeDB = plotAmplitudeDB - max(plotAmplitudeDB);

% Time of measurement
mDateVector = datevec(measurementDateNumber);
% Magic conversion found online
% "Julian Day to datenum"
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/32006
jDay = fix(measurementDateNumber) - datenum((mDateVector(1) - 1),...
    12, 31, 0, 0, 0);

figure;
plot(plotTime(noBounceI), plotAmplitudeDB(noBounceI), 'r*');
hold on
plot(plotTime(noTopI), plotAmplitudeDB(noTopI), 'b*');
plot(plotTime(topI), plotAmplitudeDB(topI), 'k*');
xlabel('Arrival time, ms');
ylabel('Arrival Amplitude, dB');

sourceH = abs( bellhopParam.sourceD - bellhopParam.bottomDepth);
recieveH = abs(bellhopParam.receiveD - bellhopParam.bottomDepth);

title({['Bellhop Arrivals, sh = ',num2str(sourceH),...
    ' m, rh = ', num2str(recieveH),' m'],...
    ['J ', num2str(jDay), '  ',...
    num2str(mDateVector(4), '%02.0f'),...
    ':', num2str(mDateVector(5), '%02.0f'), ' Zulu']});

ylim([minPlotDB - 2, 2]);
xlim([bellhopParam.tFastRange(1), bellhopParam.tFastRange(2)]);