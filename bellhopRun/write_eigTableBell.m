function write_eigTableBell(eigenRays, labels, fileName, processingInfo)

fid = fopen(fileName, 'w+');

if fid == -1
    error('File did not open');
end

fprintf(fid, 'Eigen ray tabel for ');
fprintf(fid, 'J%03d %02d:%02d\n',processingInfo.day,...
    processingInfo.hour,processingInfo.min);
fprintf(fid, 'Reciever # %01d', processingInfo.dataChannel);
fprintf(fid, '\n\n');

fprintf(fid, 'First column is arrival time.\n');
fprintf(fid, 'Remaining columns are arrival magnitude in dB re Max.\n');
fprintf(fid, '\n\n');

for i = 1 : numel(labels)
    fprintf(fid, labels{i});
    fprintf(fid, '\t');
end

fprintf(fid, '\n\n');

lastTime = 0;

for i = 1 : size(eigenRays, 1)
    row = eigenRays(i,:);
    
    % Put an extra space if row is more than 1 ms away
    if row(1) > lastTime + 0.02
        fprintf(fid, '\n');
    end
    
    for ii = 1 : numel(row);
        if ii == 1
            fprintf(fid, '%0.2f\t\t', row(ii));
        else
            if ~isnan(row(ii))
                fprintf(fid, '%0.1f', row(ii));
            else
                fprintf(fid, ' -- ');
            end
            if ii ~= numel(row)
                fprintf(fid, '\t');
            end
        end
    end
    fprintf(fid,'\n');
    lastTime = row(1);
    
end

fclose(fid);